import React from 'react';
//import HTTPService from '../../services/api'
import ApiService from '../../services/api'
import { get } from './../../config/twitter.config'
import qs from 'qs'
import {TWITTER_API} from '../../constants'

class RequestExample extends React.Component {

    constructor(props) {
        super(props);
        this.state = {

        }
    }

  componentDidMount() {
    ApiService.get('http://localhost:8080/followers', {user: 'tomaccoSan'})
      .then(response => {
        console.log(response)
      })
      .catch(error => {
        console.log(error)
      })
  }

    getSomething = async () => {

    }

    render() {
        return (
            <button type="button" onClick={this.getSomething}>Get something</button>
        );
    }
}

export default RequestExample

