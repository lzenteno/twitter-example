import React from 'react'
import './Header.scss'
import ApiService from '../../services/api'
import ContentList from '../ContentList';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user1: '',
            user2: '',
        }
    }

    getFollowers = async () => {
        const { user1, user2 } = this.state;
        ApiService.get('http://localhost:8080/followers', { first_user: user1, second_user: user2 })
            .then(response => {
                console.log(response.data);
                this.setState({
                    ...this.state,
                    commonFollowers: response.data.common_followers
                });
            })
            .catch(error => {
                console.log(error)
            })
    }

    mergeArray = (array1, array2) => {
        console.log(JSON.stringify(array1));
        let mergedArray = [];
        array1.users.map(user1 => {
            array2.users.find(function (user2) {
                if (user1.id === user2.id) {
                    mergedArray.push(user2);
                }
            });
        });
        console.log(mergedArray);
    }

    handleChange = (name, value) => {
        this.setState({
            ...this.state,
            [name]: value
        })
    }

    render() {
        const { commonFollowers } = this.state;
        return (
            <div>
                <header className="wrapper_header">
                    <div className="container">
                        <div className="box_ttls">
                            <h4 className="ttls text-center">AMIGOS EN</h4>
                            <h1 className="ttls text-center">TWITTER</h1>
                        </div>
                        <form >
                            <div className="row">
                                <div className="col-sm-5">
                                    <div className="form-group">
                                        <label for="">AMIGO 1</label>
                                        <input type="text" name="user1" className="form-control" id="" placeholder=""
                                            onChange={e => this.handleChange(e.target.name, e.target.value)} />
                                    </div>
                                </div>
                                <div className="col-sm-5">
                                    <div className="form-group">
                                        <label for="">AMIGO 2</label>
                                        <input type="text" name="user2" className="form-control" id="" placeholder=""
                                            onChange={e => this.handleChange(e.target.name, e.target.value)} />
                                    </div>
                                </div>
                                <div className="col-sm-2 mt">
                                    <button type="button" onClick={this.getFollowers} className="btn btn_go">Go</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </header>
                <ContentList
                    commonFollowers={commonFollowers}
                />
            </div>
        )
    }
}

export default Header;