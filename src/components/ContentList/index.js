import React from 'react'
import './ContentList.scss'

class ContentLit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            commonFollowers: [],
        };
    }

    componentDidMount = () => {
        this.setState({
            ...this.state,
            commonFollowers: this.props.commonFollowers
        });
    }

    componentWillReceiveProps = (objectprevProps, objectprevState) => {
        this.setState({
            ...this.state,
            commonFollowers: objectprevProps.commonFollowers
        });
    }

    render() {
        const { commonFollowers } = this.state;
        return (
            <div className="wrapper_content_list">
                <div className="container">
                    {commonFollowers ?
                        commonFollowers.map(follower => {
                            return <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <a href="#">
                                                <img class="media-object img-circle" src={follower.profile_image_url} alt="avatar" />
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <h3 class="media-heading">{follower.name}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        })
                        : ''}
                </div>
            </div>
        )
    }
}

export default ContentLit;