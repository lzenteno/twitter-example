import axios from 'axios'
import qs from 'qs'

class ApiService {

  get = (url, params) => {
    return axios.get(`${url}?${qs.stringify(params)}`)
  }
}

export default new ApiService()
